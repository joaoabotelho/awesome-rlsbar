import React from 'react'
import Pagination from 'react-bootstrap/Pagination'

export default function PaginationComp({ cardsPerPage, totalCards, paginate, activePage}) {
    let items = [];
    let active = activePage;

    for(let i =1; i <= Math.ceil(totalCards / cardsPerPage ); i++) {
        items.push(
            <Pagination.Item onClick={() => paginate(i)} key={i} active={i === active}>
                {i}
            </Pagination.Item>
        );
    }

    return (
        <div>
            <Pagination size="sm">{items}</Pagination>
        </div>
    )
}
