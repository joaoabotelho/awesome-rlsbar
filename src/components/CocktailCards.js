import React, { useState, useEffect } from 'react'
import Card from 'react-bootstrap/Card'
import CardColumns from 'react-bootstrap/CardColumns'
import Badge from 'react-bootstrap/Badge'
import PaginationComp from './PaginationComp';
import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button'

export default function CocktailCards(props) {
    const [currentPage, setCurrentPage] = useState(1);
    const [cardsPerPage] = useState(9);
    const [currentCocktails, setCurrentCocktails] = useState([])

    // Get current posts
    const indexOfLastCard = currentPage * cardsPerPage;
    const indexOfFirstCard = indexOfLastCard - cardsPerPage;
    const currentCards = currentCocktails.slice(indexOfFirstCard, indexOfLastCard);

    // Change page
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    function sortByName(){
        let cocktails = currentCocktails.sort(
            function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                if (x < y) { return -1; }
                if (x > y) { return 1; }
                return 0;
            }
        );
        setCurrentCocktails(cocktails);
    }
    
    useEffect(() => {
        function update() {
            setCurrentCocktails(props.cocktails);
        }
        update();
    }, [props])

    return (
        <div>
            <PaginationComp paginate={paginate} activePage={currentPage} cardsPerPage={cardsPerPage} totalCards={currentCocktails.length} />
            <Button onClick={() => sortByName()} variant="outline-primary">Order</Button>
            <CardColumns>
                {currentCards.map(c => (
                    <Card key={c.id} style={{ width: '18rem' }}>
                        <Card.Img variant="top" src={c.image_url} />
                        <Card.Body>
                            <Card.Title>{c.name}</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">{c.category.name}</Card.Subtitle>
                            {c.tags.map(i => (<Badge variant="primary">{i.name}</Badge>))}
                        </Card.Body>
                        <Card.Body>
                            <Link to={{
                                pathname: "/recipe",
                                state: {
                                    drink: c,
                                }
                            }}>Recipe
                            </Link>
                        </Card.Body>
                    </Card>
                ))}
            </CardColumns>
        </div >
    )
}
