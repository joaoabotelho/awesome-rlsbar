import React, { useState } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import { useHistory } from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import FormControl from 'react-bootstrap/FormControl';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';


const Search = () => {
  const history = useHistory();
  const [value, setValue] = useState('');

  const handleSumbit = (e) => {
    e.preventDefault();
    if (value && value.length > 0) {
      history.push({
        pathname: '/search',
        search: '?q=' + value,
      });
    }
  };

  const handleChange = (e) => setValue(e.target.value);

  return (
    <Form onSubmit={handleSumbit} inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={handleChange}/>
      <Button variant="outline-success" type="submit">Search</Button>
    </Form>
  );
};

const SearchByIngredient = () => {
  const history = useHistory();
  const [value, setValue] = useState('');

  const handleSumbit = (e) => {
    e.preventDefault();
    if (value && value.length > 0) {
      history.push({
        pathname: '/searchByIngredient',
        search: '?q=' + value,
      });
    }
  };

  const handleChange = (e) => setValue(e.target.value);

  return (
    <Form onSubmit={handleSumbit} inline>
      <FormControl type="text" placeholder="Search by Ingredient" className="mr-sm-2" onChange={handleChange}/>
      <Button variant="outline-success" type="submit">Search</Button>
    </Form>
  );
};

const Header = () => (
  <Navbar bg="dark" variant="dark" expand="lg" className="mb-5">
    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <LinkContainer to="/">
          <Nav.Link>Home</Nav.Link>
        </LinkContainer>
        <LinkContainer to="/cocktails">
          <Nav.Link>Cocktails</Nav.Link>
        </LinkContainer>
        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
          <NavDropdown.Item href="/random">Random Drink</NavDropdown.Item>
          <NavDropdown.Item href="/A-Z">A-Z</NavDropdown.Item>
        </NavDropdown>
      </Nav>
      <Search />
      <SearchByIngredient />
      
    </Navbar.Collapse>
  </Navbar>
);

export default Header;