/*
const DRINKS_URL = 'https://www.thecocktaildb.com/api/json/v1/1';


const objectAsParams = (obj) => Object.keys(obj).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key])
}).join('&');


// https://www.thecocktaildb.com/api.php

export const getRandomDrink = () =>
  fetch(`${DRINKS_URL}/random.php`)
    .then(r => r.json())
    .then(d => d.drinks[0]);

export const searchDrink = (i) =>
  fetch(`${DRINKS_URL}/search.php${objectAsParams({ i })}`)
    .then(r => r.json())
    .then(d => d.drinks[0])

export const getCategories = () =>
  fetch(`${DRINKS_URL}/list.php?c=list`)
    .then(r => r.json())
    .then(d => d.drinks.map(e => e.strCategory));

export const getIngredients = () =>
  fetch(`${DRINKS_URL}/list.php?i=list`)
    .then(r => r.json())
    .then(d => d.drinks.map(e => e.strIngredient1));

export const getDrinkInCategory = (c) =>
  fetch(`${DRINKS_URL}/filter.php?c=${c}`)
    .then(r => r.json())
    .then(d => d.drinks);

*/
import axios from "axios"

const DRINKS_URL = 'http://localhost:3001';

export const getCocktails = () =>
  axios.get(`${DRINKS_URL}/cocktails`)
  .then(r => r.data)

export const getRandomDrink = () =>
  axios.get(`${DRINKS_URL}/cocktails/random`)
    .then(r => r.data)

export const searchDrink = (i) =>
  axios.get(`${DRINKS_URL}/cocktails?name=${i}`)
    .then(r => r.data)

export const searchDrinkByIngredient = (i) =>
  axios.get(`${DRINKS_URL}/cocktails?ingredient=${i}`)
    .then(r => r.data)

export const getCategories = () =>
  axios.get(`${DRINKS_URL}/categories`)
    .then(r => r.data)

export const getIngredients = () =>
  fetch(`${DRINKS_URL}/ingredients`)
    .then(r => r.json())
    .then(d => d.drinks.map(e => e.strIngredient1));

export const getDrinkInCategory = (c) =>
  axios.get(`${DRINKS_URL}/cocktails?category_id=${c}`)
    .then(r => r.data);

export const getDrinkStartsWith = (l) =>
  axios.get(`${DRINKS_URL}/cocktails?starts_with=${l}`)
    .then(r => r.data);