import React, { useState, useEffect } from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Nav from 'react-bootstrap/Nav';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import { Link } from "react-router-dom";

import { getCategories } from '../api';

const Home = () => {
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function getData() {
      setLoading(true);
      const cats = await getCategories();
      setCategories(cats);      
      setLoading(false);
    }
    getData();
  }, []);

  return (
    <Row>
      <Col sm={8}>
        <Jumbotron>
          <h1 className="header">RedLight Bar</h1>
        </Jumbotron>
      </Col>
      <Col sm={4}>
        <Nav className="flex-column">
          {loading && (
            <Spinner animation="border" role="status" className="mx-auto">
              <span className="sr-only">Loading...</span>
            </Spinner>
          )}
          {categories.map(c => (
            <Nav.Item key={c.id}>
              <Link to={{
                pathname: "/cocktail-list",
                state: {
                  categoryId: c.id,
                  categoryName: c.name
                }
              }}>{c.name}
                </Link>
            </Nav.Item>
          ))}
        </Nav>
      </Col>
    </Row>
  );
}

export default Home;