import Drink from './Drink'
import React, { useState, useEffect }from 'react'
import Spinner from 'react-bootstrap/Spinner';
import { getRandomDrink } from '../api';

export default function Random() {
    const [drink, setDrink] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function getData() {
            setLoading(true);
            const drink =  await getRandomDrink();
            setDrink(drink);
            setLoading(false);
        }
        getData();
    }, [])

    return (
        <div>
            {loading && (
                <Spinner animation="border" role="status" className="mx-auto">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )}
            <Drink cocktail={drink} />
        </div>
    )
}
