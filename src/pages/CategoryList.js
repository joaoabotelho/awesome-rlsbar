import React, { useState, useEffect } from 'react'
import { useLocation, useHistory } from "react-router-dom";
import Spinner from 'react-bootstrap/Spinner';

import { getDrinkInCategory } from '../api';
import CocktailCards from '../components/CocktailCards';
import Button from 'react-bootstrap/Button'

const CategoryList = () => {
    const [loading, setLoading] = useState(false);
    const category = useLocation().state.categoryName;
    const categoryId = useLocation().state.categoryId;
    const [cocktails, setCocktails] = useState([]);
    const history = useHistory();

     useEffect(() => {
        async function getData() {
            setLoading(true);
            const c = await getDrinkInCategory(categoryId);
            setCocktails(c);
            setLoading(false);
        }
        getData();
    }, [categoryId]);

    return (
        <div>
            <h1>Category: {category}</h1>
            <Button onClick={() => history.goBack()} variant="outline-primary">Back</Button>
            {loading && (
                <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            <CocktailCards cocktails={cocktails} />
        </div>
    )
}

export default CategoryList;
