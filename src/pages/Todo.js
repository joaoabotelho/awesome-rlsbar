import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

import { saveTodosToStorage, loadTodosFromStorage } from '../utils';

const TODOS_EXEMPLO = [
  "1. Página para listar as letras do alfabeto (A-Z), clicando numa letra pesquisa bebidas que comecem pela mesma",
  "2. Página individual de cocktail",
  "2.1. Linkar ingrediente para página individual do mesmo",
  "2.2. Input de texto para filtrar ingredientes localmente",
  "3. Página de categoria individual que lista os cocktails (listagem)",
  "3.1. Possibilidade de ordenar lista por ordem alfabetica (numa tabela)"
];


class Todo extends React.PureComponent {
  state = {
    todos: TODOS_EXEMPLO.map(label => ({ label, checked: false })),
  }

  componentDidMount() {
    const storedTodos = loadTodosFromStorage();
    if (storedTodos) {
      this.setState({ todos: storedTodos });
    }
  }

  componentDidUpdate() {
  }

  handleTodoChange = index => (e) => {
    const checked = e.target.checked;
    const todo = this.state.todos[index];

    if (!todo) return;

    let newList = [...this.state.todos];
    newList[index] = { ...todo, checked };
    this.setState({ todos: newList });
    saveTodosToStorage(newList);
  }

  render() {
    return (
      <Row>
        <Col>
         <h1>Todo</h1>
          <div key="default-checkbox" className="mb-5">
            {this.state.todos.map((t, index) => (
              <Form.Check
                key={`t-${index}`}
                type="checkbox"
                id={`t-${index}`}
                label={t.label}
                onChange={this.handleTodoChange(index)}
                checked={t.checked}
                value={t.checked}
              />
            ))}
          </div>
        </Col>
      </Row>
    );
  }
}

export default Todo;