import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';

import CocktailCards from '../components/CocktailCards';

import { getCocktails } from '../api';

class Cocktails extends React.Component {
  state = {
    loading: false,
    cocktails: [],
  }

	async componentDidMount() {
    this.setState({ loading: true });
    const data = await getCocktails();
    this.setState({ loading: false, cocktails: data });
  }

  render() {
    const { loading, cocktails } = this.state;
    return (
      <div>
        <Jumbotron>
          <h1 className="header">Cocktails</h1>
        </Jumbotron>
        <Container>
          <Row>
            <Col>
              {loading && (
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              )}
              <CocktailCards cocktails={cocktails} />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Cocktails;