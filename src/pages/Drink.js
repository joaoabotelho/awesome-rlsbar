import React, { useState, useEffect } from 'react'
import { useLocation, useHistory } from "react-router-dom";
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import { Row, Col } from 'react-bootstrap'
import ListGroup from 'react-bootstrap/ListGroup'
import Badge from 'react-bootstrap/Badge'
import Button from 'react-bootstrap/Button'


export default function Drink(props) {
    const state = useLocation().state
    const [cocktail, setCocktail] = useState(state ? state.drink : []);
    const history = useHistory();
    
    useEffect(() => {
        function update() {
            setCocktail(state ? state.drink : props.cocktail);
        }
        update();
    }, [state, props])
    const measures = cocktail.measures ? cocktail.measures : null;

    const ingredients = cocktail.ingredients ? (cocktail.ingredients.map((i, index) => (
        <ListGroup.Item key={i.id}>{measures[index].value} {i.name}</ListGroup.Item>)
    )) : null;

    const category = cocktail.category ? (<h3>{cocktail.category.name}</h3>) : null;
    
    const tags = cocktail.tags ? (cocktail.tags.map(i => (<Badge variant="primary">{i.name}</Badge>))): null;

    return (
        <div>
            <Button onClick={() => history.goBack()} variant="outline-primary">Back</Button>
            <Container>
                <Row>
                    <Col xs={6} md={4}>
                        <Image src={cocktail.image_url} fluid />
                    </Col>
                    <Col xs={6} md={4}>
                        <h1>Hello {cocktail.name}!</h1> 
                        {category}
                        {tags}
                        <ListGroup variant="flush">
                        <p>
                           {cocktail.instructions} 
                        </p>
                        {ingredients}
                    </ListGroup>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
