import React, { useState } from 'react'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import { getDrinkStartsWith } from '../api';
import CocktailCards from '../components/CocktailCards';
import Spinner from 'react-bootstrap/Spinner';


export default function AZ() {
    const ascii_uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    const [cocktails, setCocktail] = useState([])
    const [loading, setLoading] = useState(false);


    function updateCocktailList(c){
        async function getData() {
            setLoading(true);
            const drinks = await getDrinkStartsWith(c);
            setCocktail(drinks);
            setLoading(false);
        }
        getData();
    }

    const dropdown_items = ascii_uppercase.split('').map(c => (
        <Dropdown.Item key={c} onClick={() => updateCocktailList(c)} eventKey={c}>{c}</Dropdown.Item>
    ))

    return (
        <div>
            <ButtonGroup>
                <DropdownButton as={ButtonGroup} title="Dropdown" id="bg-nested-dropdown">
                   {dropdown_items} 
                </DropdownButton>
            </ButtonGroup>
            {loading && (
                <Spinner animation="border" role="status" className="mx-auto">
                <span className="sr-only">Loading...</span>
              </Spinner>
            )}
            <CocktailCards cocktails={cocktails} />
        </div>
    )
}
