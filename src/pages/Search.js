import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import { withRouter } from 'react-router-dom';
import { searchDrink, searchDrinkByIngredient } from '../api';
import CocktailCards from '../components/CocktailCards';


class Search extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {
      cocktails : [],
      searchQuery: "",
      ingredient: props.ingredient
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ingredient !== this.props.ingredient){
      this.setState({
        ingredient: this.props.ingredient
      })
    } else {
      this.processSearchParam();
    }
  }

  processSearchParam() {
    const { location } = this.props;
    const qs = new URLSearchParams(location.search);
    const searchQuery = qs.get('q');

    if (searchQuery) {
      if (this.state.ingredient) {
        searchDrinkByIngredient(searchQuery).then(result => {
          if (this.state.searchQuery !== searchQuery) {
            this.setState({
              cocktails: result,
              searchQuery: searchQuery,
              ingredient: true
            });
          }
        });
      } else {
        searchDrink(searchQuery).then(result => {
          if (this.state.searchQuery !== searchQuery) {
            this.setState({
              cocktails: result,
              searchQuery: searchQuery,
              ingredient: false
            });
          }
        });
      }
    }
  }

  render() {
    return (
      <div>
        <Jumbotron>
          <h1 className="header">RedLight Bar</h1>
        </Jumbotron>
        <CocktailCards cocktails={this.state.cocktails} />
      </div>
    );
  }
}

export default withRouter(Search);