import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Container from 'react-bootstrap/Container';

import Home from './pages/Home';
import CategoryList from './pages/CategoryList';
import Random from './pages/Random';
import AZ from './pages/AZ';
import Drink from './pages/Drink';
import Search from './pages/Search';
import Cocktails from './pages/Cocktails';
import Header from './components/Header';


function App() {
  return (
    <BrowserRouter>
      <Container className="p-3">
        <Header />
        <Switch>
          <Route path="/cocktails">
            <Cocktails />
          </Route>
          <Route path="/search">
            <Search ingredient={false} />
          </Route>
           <Route path="/searchByIngredient">
            <Search ingredient={true} />
          </Route>
          <Route name="recipe" path="/recipe">
            <Drink />
          </Route>
          <Route name="random" path="/random">
            <Random />
          </Route>
          <Route name="AZ" path="/A-Z">
            <AZ />
          </Route>
          <Route name="cocktail-list" path="/cocktail-list">
            <CategoryList />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
